FROM node:10.16.3

# Create app directory
WORKDIR /usr/src/app

RUN npm install -g serve
# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

RUN npm install

# Bundle app source
COPY . .

RUN npm run build

EXPOSE 80

CMD [ "serve", "-l", "80", "-s", "dist" ]