import Vue from 'vue';
import Router from 'vue-router';
import GoogleVisionTest from './views/GoogleVisionTest.vue';
import AR from './views/AR.vue';
import ARdev from './views/AR_dev.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/google_vision',
      name: 'google_vision',
      component: GoogleVisionTest,
    },
    {
      path: '/',
      name: 'ar',
      component: AR,
    },
    {
      path: '/ardev',
      name: 'ardev',
      component: ARdev,
    },
  ],
});
