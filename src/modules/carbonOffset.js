const PRIVATE_KEY = 'c03244e7349de2ce';

export default async (co2Gram) => {
  let request;
  let URL;

  if (co2Gram >= 1000) {
    URL = 'https://climate-api-test.dakar.moccu.net/api/orders?api-key=2947ee2d-bca1-4bc4-aa81-017ca40cb5b3';

    const body = {
      client: 'carbon-reporter',
      order: 'carbon-reporter',
      emissions: co2Gram / 1000,
      project_id: 1047,
    };

    request = {
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
      method: 'POST',
    };
  } else {
    URL = 'https://api.cloverly.com/2019-03-beta/estimates/carbon';

    request = {
      body: `{"weight":{"value":${co2Gram},"units":"grams"}}`,
      headers: {
        Authorization: `Bearer private_key:${PRIVATE_KEY}`,
        'Content-Type': 'application/json',
      },
      method: 'POST',
    };
  }

  try {
    const response = await fetch(URL, request);
    const responseJSON = (await response.json());
    if (co2Gram >= 1000) {
      return responseJSON.order_price * 1.1;
    }
    return responseJSON.rec_cost_in_usd_cents / 100;
  } catch (error) {
    console.log(error);
    return null;
  }
};
