export class Camera {
  videoElement;

  snapShotCanvas;

  videoStreamSettings;

  constructor(videoElement) {
    this.videoElement = videoElement;
    this.snapShotCanvas = document.createElement('canvas');
  }

  async setupCamera() {
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      const stream = await navigator.mediaDevices.getUserMedia({
        audio: false,
        video: { facingMode: 'environment' },
      });
      this.videoStreamSettings = stream.getVideoTracks()[0].getSettings();
      window.stream = stream;
      this.videoElement.srcObject = stream;
      return new Promise((resolve) => {
        this.videoElement.onloadedmetadata = () => {
          resolve(this.videoElement.videoWidth, this.videoElement.videoHeight);
        };
      });
    }
    return null;
  }

  snapshot() {
    this.snapShotCanvas.height = this.videoStreamSettings.height;
    this.snapShotCanvas.width = this.videoStreamSettings.width;
    const ctx = this.snapShotCanvas.getContext('2d');
    ctx.drawImage(this.videoElement, 0, 0, this.snapShotCanvas.width,
      this.snapShotCanvas.height);
    const img = new Image();
    img.src = this.snapShotCanvas.toDataURL('image/png');
    return img;
  }
}

export default Camera;
