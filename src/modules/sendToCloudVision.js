const apiKey = 'AIzaSyAOre66eAJbbntYVvJ5FwMn-eVFaNcPMZM';
const CV_URL = `https://vision.googleapis.com/v1/images:annotate?key=${apiKey}`;

export default async (content) => {
  const request = {
    requests: [{
      image: {
        content,
      },
      features: [{
        type: 'LABEL_DETECTION',
        maxResults: 200,
      }],
    }],
  };

  try {
    const response = await fetch(CV_URL, {
      body: JSON.stringify(request),
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return await response.json();
  } catch (error) {
    console.log(error);
    return null;
  }
};
